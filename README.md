# TD 1 - Git/Gitlab

## Exercice 1 - Installation

1. Installer le logiciel GIT
2. Vérifier la version de GIT installer sur vos poste de travail ( git --version )

![Image title](screenshots/Exo_2_ .png)


## Exercice 2 - Configuration & création d’un repository

1. Initialiser votre nom & email. ( user.name & user.email ).

    Initialisation effectuée, la preuve est que je peux commit/push etc vers cette page !

2. Créer un répertoire “training-git”.
3. Initialiser votre repository à l’intérieur de ce dossier.
4. Vérifier que le repository est correctement créé ( présence d’un certain dossier cacher.. )

Les réponses à ces questions sont toutes dans la capture d'écran :

![Image title](screenshots/Exo_2_ .png)


## Exercice 3 - 1er Commit

1. Dans le répertoire “training-git” créer un fichier index.html.
2. Insérer dans ce fichier la déclaration du doctype HTML.
3. Ajoutez le fichier index.html dans le stage.
4. Créer un commit avec ce fichier, en respectant la norme de nommage du commit comme
vu en cours.
5. Vérifier que le commit ai bien été ajouté dans l’historique des commit de votre repository.
6. Modifier le fichier index.html
7. Comité à nouveau ce fichier en utilisant VIM


Les réponses à ces questions sont dans les captures d'écran suivantes :

![Image title](screenshots/Exo 3 q1 q3.png)
![Image title](screenshots/Exo 3 q4 q5.png)
![Image title](screenshots/Exo 3 q6 q7.png)


## Exercice 5 - Merge

1. Créer un dossier training-merge.
2. Initialiser votre git.
3. Créer un fichier index.html est commit le.
4. Créez une branche “ feature-first-merge ” et déporte-toi dessus.
5. Créer un fichier main.js
6. Commité ce.
7. Rend toi sur la branche master et assure toi que ce fichier n’est pas présent.
8. Mergé la branche “feature-first-merge” sur master.
9. Vérifie que le code le commit fait sur feature-first-merge est bien présent sur master

Les réponses à ces questions sont dans les captures d'écran suivantes :

![Image title](screenshots/Exo 5 q1 q8.png)
![Image title](screenshots/First merge.png)


## Exercice 6 - Conflict

1. Créer un dossier training-conflict
2. Initialiser votre git
3. Créer un fichier index.html & commit le
4. Créer une branche “feature-first-conflict” et déporte-toi dessus.
5. Modifie le fichier index.html pour lui ajouter un titre <title> à la ligne 1
6. Commit cette modification
7. Rend toi sur la branche master
8. Modifie le fichier index.html pour lui ajouter un <body> à la ligne 1
9. Commit cette modification
10. Merge la branche feature-first-conflict sur master.

Les réponses à ces questions sont dans les captures d'écran suivantes :

![Image title](screenshots/exercice 6 git conflict q1 q9.png)
![Image title](screenshots/exercice 6 résolution conflit.png)


## Exercice 8 - Merge Request ( MR )

1. Rendez-vous sur la branche master du projet training-git.
2. Créer une branche first-merge-request.
3. Pousse cette branche sur ton repository distant ( git push )
4. Créer un fichier main.js
5. Ajouter le code console.log(“premiere MR”) à l’intérieur de celui-ci
6. Commit le et pousse le sur ton repository distant ( git push )
7. Rend-toi sur gitlab ( connecte-toi si besoin )
8. Rend-toi sur ton projet training-git.
9. Rend-toi sur la page branche est assure toi que ta nouvelle branche “first-merge-request”
est bien présente.
10. Créer une nouvelle MR en sélectionnant le branche first-merge-request en “source
branch” et master en “target branche”
11. Une fois que tu t’es assuré que tes développements sont conformes aux attentes ( onglet
change de la page merge request ), tu peux cliquer sur merge

Les réponses à ces questions sont dans les captures d'écran suivantes :

![Image title](screenshots/exo 8 modification vers main OK.png)
![Image title](screenshots/exo8 merged.png)


##Exercice 9 - Créer un gitflow
Grâce aux compétences acquises durant le cours et durant ce TP, réalise le GitFlow suivant, en
respectant les indications ci-dessous :
- Créer un commit sur la branche master avant de tirer la branche develop
- La branche develop est tirée à partir de master.
- Ne te préoccupe pas des tags ( v1.0.0 v1.1.0 ) pour cet exercice.
- Le commit violet sera une feature ( se référer au nommage des commits suivant la
documentation relative à ce sujet )
- Le commit orange sera une feature
- Les deux commits vert sont des fixs
- Les commits de merge sont générés automatiquement par gît au moment du merge de
vos développement.

Certaines réponses se trouvent directement dans la branche créée pour l'exercice et pour la version full git : 

![Image title](screenshots/exo 9.png)
![Image title](screenshots/exo 9 feature orange et correction bug 1.png)
![Image title](screenshots/exo 9 correction bug 2 et merge.png)